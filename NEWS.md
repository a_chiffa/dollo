
# Versions

## 0.3.2

- Fixed bug during annotation.

## 0.3.1

- Added support for `weight` column in the input file. Can be used to speed up likelihood calculation if multiple sites have the same data.

## 0.3.0

- Added maximum likelihood estimates of presence, loss and origin.

## 0.2.3

- Fixed numerical precision issued caused by switching to scipy logsumexp

## 0.2.2

- Fixed big in loss anotation

- Added support for newick string

## 0.2.1

- Fixed bug in presence annotation

## 0.2.0

- Added ability to optimise probability of loss

## 0.1.3

- Fixed a bug when pi_l was not in (0, 1]

## 0.1.2

- Fixed bug in annotation

## 0.1.1

- Fixed bug causing names of leafs to be swapped during computation

## 0.1.0

- Initial release

