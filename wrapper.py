from pipelines.io import get_ancestor_directory, make_directory, make_parent_directory
from pipelines.job_manager import get_job_managers 
from pipelines.ruffus_helpers import run_pipeline
from pipelines.utils import get_sample_information
 
from ruffus import *
from ruffus.combinatorics import product

import gzip
import numpy as np
import pandas as pd
import os
import shutil

cwd = os.path.dirname(os.path.realpath(__file__))

#=======================================================================================================================
# Read Command Line Input
#=======================================================================================================================
parser = cmdline.get_argparse(description='''Wrapper for stochastic Dollo phylogentic inference.''')

parser.add_argument('--in_file', required=True)

parser.add_argument('--out_dir', required=True)

# Pipeline arguments
parser.add_argument('--num_cpus', default=1, type=int,
                    help='''Number of jobs to run.''')

args = parser.parse_args()

#=======================================================================================================================
# Global variables
#=======================================================================================================================
convert_to_newick_script = os.path.join(cwd, 'bin', 'convert_to_newick.py')

tmp_dir = os.path.join(args.out_dir, 'tmp')

results_dir = os.path.join(args.out_dir, 'results')

trees_dir = os.path.join(tmp_dir, 'trees')

tree_pickle_file = os.path.join(tmp_dir, 'ml_tree.pickle.gz')

node_file = os.path.join(results_dir, 'nodes.tsv.gz')

search_file = os.path.join(results_dir, 'search.tsv.gz')

tree_file = os.path.join(results_dir, 'tree.nwk')

#=======================================================================================================================
# Pipeline
#=======================================================================================================================
@subdivide(args.in_file,
           formatter(),
           [os.path.join(trees_dir, '*.pickle.gz'), os.path.join(trees_dir, 'done.txt')],
           trees_dir)
def create_trees(in_file, out_files, out_dir):
    make_directory(out_dir)
    
    cmd = 'PyDollo'
    
    cmd_args = [
                'create_trees',
                '--log_likelihoods_file', in_file,
                '--out_dir', out_dir
                ]
    
    run_cmd(cmd, cmd_args)
    
    open(out_files[-1], 'w').close()

@transform(create_trees, 
           formatter('trees/(.*)\.pickle\.gz'),
           add_inputs(args.in_file), 
           '{subpath[0][1]}/tree_log_likelihoods/tree_{1[0]}.prob_optimal.tsv.gz')
def compute_tree_log_likelihoods((tree_file, log_likelihoods_file), out_file):
    make_parent_directory(out_file)

    tmp_file = out_file + '.tmp'

    cmd = 'PyDollo'
    
    cmd_args = [
                'compute_tree_log_likelihood',
                '--log_likelihoods_file', log_likelihoods_file,
                '--tree_file', tree_file,
                '--out_file', tmp_file,
                '--max_probability_of_loss', 0.5,
                '--min_probability_of_loss', 0.0
                ]
    
    run_cmd(cmd, cmd_args)
    
    shutil.move(tmp_file, out_file)

@merge(compute_tree_log_likelihoods, search_file)
def build_results_file(in_files, out_file):
    make_parent_directory(out_file)
    
    data = []
    
    for file_name in in_files:
        info = get_sample_information(file_name)
        
        df = pd.read_csv(file_name, compression='gzip', sep='\t')

        row = {
               'tree_id' : info['tree'],
               'probability_of_loss' : df.at[0, 'probability_of_loss'],
               'log_likelihood' : df.at[0, 'log_likelihood']
               }
        
        data.append(row)
    
    data = pd.DataFrame(data, columns=['tree_id', 'probability_of_loss', 'log_likelihood'])
    
    data = data.sort('log_likelihood', ascending=False)
    
    with gzip.GzipFile(out_file, 'w') as out_fh:
        data.to_csv(out_fh, index=False, sep='\t')
        
@transform(build_results_file, formatter(), tree_pickle_file)
def find_ml_tree(in_file, out_file):
    make_parent_directory(out_file)
    
    df = pd.read_csv(in_file, compression='gzip', sep='\t')
    
    row = df.iloc[df.log_likelihood.idxmax()]
    
    best_file = os.path.join(trees_dir, '{0}.pickle.gz'.format(int(row['tree_id'])))
    
    shutil.copyfile(best_file, out_file)

@transform(find_ml_tree, formatter(), add_inputs(args.in_file), node_file)
def annotate_posteriors((tree, likelihood), out_file):
    make_parent_directory(out_file)
    
    df = pd.read_csv(search_file, compression='gzip', sep='\t')
    
    row = df.iloc[df.log_likelihood.idxmax()]
    
    cmd = 'PyDollo'
    
    cmd_args = [
                'annotate_posteriors',
                '--log_likelihoods_file', likelihood,
                '--tree_file', tree,
                '--out_file', out_file,
                '--probability_of_loss', row['probability_of_loss']
                ]
                
    run_cmd(cmd, cmd_args)
    
@transform(find_ml_tree, formatter(), tree_file)
def write_ml_tree_to_newick(in_file, out_file):
    make_parent_directory(out_file)
    
    cmd = 'python'
    
    cmd_args = [
                convert_to_newick_script,
                '--in_file', in_file,
                '--out_file', out_file,
                ]
                
    run_cmd(cmd, cmd_args)

#=======================================================================================================================
# Run Pipeline
#=======================================================================================================================
log_dir = os.path.join(tmp_dir, 'log')

run_local_cmd, run_cmd, local_job_manager, job_manager = get_job_managers(True, log_dir)

run_pipeline(args, job_manager, multithread=args.num_cpus)
