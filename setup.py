import numpy
import versioneer
from setuptools import setup, find_packages, Extension
from Cython.Build import cythonize


extensions = [
    Extension(
        name='dollo.recursion',
        sources=['dollo/recursion.pyx'],
        include_dirs=[numpy.get_include()],
    ),
]

setup(
    name='PyDollo',
    packages=find_packages(),
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description='Simple stochastic Dollo phylogenetic model for analysing cancer genomics data.',
    author='Andrew McPhershon, Andrew Roth',
    author_email='andrew.mcpherson@gmail.com, andrewjlroth@gmail.com',
    url='http://compbio.bccrc.ca',
    scripts=['PyDollo'],
    ext_modules=cythonize(extensions),
)

