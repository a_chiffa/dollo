
# Authors

Andrew McPhershon and Andrew Roth

# Installation

1. Unpack lib/PyDollo-0.3.2.tar.gz.

2. Enter the created directory and execute `python setup.py install`.

## Dependencies

- Python >= 2.7.5 (https://www.python.org/)

- numpy >= 1.9 (http://www.numpy.org/)

- scipy >= 0.14 (http://www.scipy.org/)

- pandas >= 0.15 (http://pandas.pydata.org/)

- ruffus >= 2.5  (www.ruffus.org.uk/)

- pipelines == 0.2.7 (https://bitbucket.org/aroth85/pipelines/downloads/Pipelines-0.2.7.tar.gz)

# Running

## Input

The input file for a PyDollo analysis is gzip compressed tab delimited file with a header.
The columns of the file are

- event_id - Unique identifier for event.

- site_id - Sampled id were tissue was taken from.

- log_likelihood_absent - Log likelihood that the event is absent at a site.

- log_likelihood_present - Log likelihood that the event is present at a site.

It is up to the user to compute these values. 
For binary presence absence data '-inf' (log(0)) can be used to encode absence and 0 (log(1)) to encode presence. 

## Analysis
The main software `PyDollo` will be available on the command line.
A typical analysis with PyDollo would consist of the following steps.

1. Use the `create_trees` command to generate all trees.

2. For each tree run `compute_tree_log_likelihood` to compute the log likelihood.

3. Find the maximum likelihood tree and run `annotate_posteriors` to annotated the origin, loss, presence values for each SNV.

To make this easier we have included a script `wrapper.py` in the root directory.
We have also included some example input data for snvs and breakpoints in the examples/ directory.
To perform all the steps outlined above on the snv data run the following command

`python wrapper.py --in_file examples/snvs.tsv.gz --out_dir examples/output`

if you have a multi-core computer (with x cores) you can speed up this analysis using

`python wrapper.py --in_file examples/snvs.tsv.gz --out_dir examples/output --num_cpus x`

## Output

Running the previous command will create the following output

examples/output/tmp - Directory with intermediate results.

examples/output/results - Directory with final results

examples/output/results/nodes.tsv.gz - Maximum likelihood estimates of presence (ml_presence), origin (ml_origin) and loss (ml_loss) for each event at each node on the maximum likelihood tree. This file is tab delimited with header and gzip compressed.

examples/output/results/search.tsv.gz - List of trees, maximum likelihood probability of loss for tree and maximum log likelihood for the tree. This file is tab delimited with header and gzip compressed.

examples/output/results/treek.nwk - Maximum likelihood tree stored in newick format.

examples/output/tmp/trees - Trees stored in a binary format. Ids correspond to those in examples/output/results/search.tsv.gz. These trees can be converted to newick using the bin/convert_to_newick.py script.


