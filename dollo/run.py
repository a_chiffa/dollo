'''
Created on 2014-09-24

@author: Andrew Roth
'''
import gzip
import os
import numpy as np
import pandas as pd
import pickle
import scipy.optimize as op

import dollo.recursion as recursion
import dollo.trees as trees


def _prepare_log_likelihoods(log_likelihoods, tree):
    ll_absent = log_likelihoods.set_index(['event_id', 'site_id'])['log_likelihood_absent'].unstack()
    ll_present = log_likelihoods.set_index(['event_id', 'site_id'])['log_likelihood_present'].unstack()
    
    ll_absent, ll_present = ll_absent.align(ll_present)
    
    assert (ll_absent.columns == ll_present.columns).all()
    assert (ll_absent.index == ll_present.index).all()
    assert ll_absent.notnull().all().all()
    assert ll_present.notnull().all().all()
    
    ll = np.zeros(ll_absent.shape + (2,))
    
    ll[:, :, 0] = ll_absent.values
    ll[:, :, 1] = ll_present.values
    
    leaf_names = ll_absent.columns
    variant_ids = ll_absent.index
    
    for node in tree.nodes:
        node.leaf_idx = -1
        
    assert len(leaf_names) == len(list(tree.leaves))
    
    for leaf_idx, leaf_name in enumerate(leaf_names):
        for leaf in tree.leaves:
            if leaf.name == leaf_name:
                leaf.leaf_idx = leaf_idx

    return ll, tree, leaf_names, variant_ids


def compute_tree_log_likelihood(log_likelihoods, tree, **args):
    """ Likelihood computation on tree.
    """
    
    min_probability_of_loss = args['min_probability_of_loss']
    max_probability_of_loss = args['max_probability_of_loss']
    
    ll, tree, leaf_names, variant_ids = _prepare_log_likelihoods(log_likelihoods, tree)
    
    fixed_tree = recursion.FixedTreeNode(tree)
    
    weights = np.ones((ll.shape[0],))
    
    if args.get('probability_of_loss', None) is None:
        def calc_nll(params, fixed_tree):
            loss_prob = params
            
            for node in fixed_tree.nodes:
                node.loss_prob = loss_prob
                
            return -1 * recursion.tree_log_likelihood(fixed_tree, ll, weights)

        loss_probs_init = [0.5 * (min_probability_of_loss + max_probability_of_loss)]
        loss_probs_bounds = [(min_probability_of_loss, max_probability_of_loss)]
        
        result = op.minimize(
            calc_nll,
            loss_probs_init,
            bounds=loss_probs_bounds,
            args=(fixed_tree,),
        )
        
        tree_ll = -1 * result.fun
        
        loss_prob = result.x[0]
        
    else:
        loss_prob = args['probability_of_loss']

        for node in fixed_tree.nodes:
            node.loss_prob = loss_prob

        tree_ll = recursion.tree_log_likelihood(fixed_tree, ll, weights)

    for node in tree.nodes:
        node.loss_prob = loss_prob
        
    results = {
        'tree': tree,
        'log_likelihood': tree_ll,
    }
    
    return results


def annotate_posteriors(log_likelihoods, tree):
    """ Annotation of posteriors.
    """
    
    ll, tree, leaf_names, variant_ids = _prepare_log_likelihoods(log_likelihoods, tree)
   
    annotations = {
                  'loss' : _compute_posterior(ll, tree, variant_ids, recursion.posterior_loss),
                  'origin': _compute_posterior(ll, tree, variant_ids, recursion.posterior_origin),
                  'presence' : _compute_posterior(ll, tree, variant_ids, recursion.posterior_present)
                  }

    annotations.update(compute_max_likelihood_indicators(ll, tree, variant_ids))

    nodes_table = pd.DataFrame(annotations)
    
    nodes_table.reset_index(inplace=True)
    
    return nodes_table


def create_node_series(records, index):
    df = pd.DataFrame(records, index=index)
    
    df.columns.name = 'node'
    
    df =  df.stack()
    
    return df


def compute_max_likelihood_indicators(ll, tree, variant_ids):
    fixed_tree = recursion.FixedTreeNode(tree)

    origins = []
    presences = []
    losses = []

    for idx in xrange(ll.shape[0]):
        origin, presence, loss = recursion.max_likelihood_origin_presence_loss(
            fixed_tree, ll[idx, :, :])

        origins.append(origin)
        presences.append(presence)
        losses.append(loss)

    origins = create_node_series(origins, variant_ids) * 1
    presences = create_node_series(presences, variant_ids) * 1
    losses = create_node_series(losses, variant_ids) * 1

    return dict({'ml_origin':origins, 'ml_presence':presences, 'ml_loss':losses})


def _compute_posterior(ll, tree, variant_ids, posterior_func):
    fixed_tree = recursion.FixedTreeNode(tree)

    posterior = []
    
    for idx in xrange(ll.shape[0]):
        value = posterior_func(fixed_tree, ll[idx, :, :])
        
        posterior.append(value)

    posterior = create_node_series(posterior, variant_ids)
    
    return posterior

